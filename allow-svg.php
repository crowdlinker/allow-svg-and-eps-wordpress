<?php

/**
 * Allows SVG and EPS Uploading
 */
function custom_upload_mimes( $upload_mimes ) {
    $upload_mimes['svg'] = 'image/svg+xml';
    $upload_mimes['svgz'] = 'image/svg+xml';
    $upload_mimes['eps'] = 'application/postscript';

    return $upload_mimes;
}
add_filter( 'upload_mimes', 'custom_upload_mimes', 10, 1 );